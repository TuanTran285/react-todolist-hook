import ToDoList from './ToDoList/ToDoList';

function App() {
  return (
    <div className=''>
      <ToDoList/>
    </div>
  );
}

export default App;
