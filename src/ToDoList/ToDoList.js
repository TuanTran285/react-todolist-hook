import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useFormik } from 'formik'
import * as Yup from "yup";
import { addTaskAction, checkTaskAction, editTaskAction, removeTaskAction, updateTaskAction } from '../redux/actions/ToDoListAction';
export default function ToDoList() {
  let { taskList } = useSelector(state => state.ToDoListReducer)
  let dispatch = useDispatch()
  const [currentTask, setCurrentTask] = useState(null)
  const [disableAddTask, setDisableAddTask] = useState(false)
  const [disableUpdateTask, setDisableUpdateTask] = useState(false)
  const formik = useFormik({
    initialValues: {
        taskName: '',
    },
    validationSchema: Yup.object({
      taskName: Yup.string().required('Taskname isValid')
    }),
    onSubmit: (values, actions) => {
      let index = taskList.findIndex(item => {
        return item.taskName.toLowerCase() === values.taskName.toLowerCase()
      })
      if (index === -1) {
        dispatch(addTaskAction({ ...values, taskId: new Date(), done: false }))
      } else {
        alert('Đã có task này')
        return
      }
      actions.resetForm()
    },
  });

  const removeTask = (taskId) => {
    dispatch(removeTaskAction(taskId))
  }
  const editTask = (task) => {
    formik.setValues({
      taskName: task.taskName
    })
    setCurrentTask(task.taskId)
    setDisableAddTask(true)
    setDisableUpdateTask(false)

  }
  const updateTask = () => {
    dispatch(updateTaskAction(currentTask, formik.values.taskName))
    formik.resetForm()
    setDisableAddTask(false)
    setDisableUpdateTask(true)
  }
  const checkTask = (taskId) => {
    dispatch(checkTaskAction(taskId))
  }

  const handleSetTheme = (e) => {
    switch (e.target.value) {
      case 'dark': {
        document.body.classList.add('dark')
        return
      }
      case 'light': {
        document.body.classList.remove('dark')
        return
      }
      default: {
        return
      }
    }
  }
  // render todoList chưa làm
  const renderTaskToDo = () => {
    return taskList.filter(item => !item.done).map((item, index) => {
      return <div key={index} className='border-[1px] p-2 flex justify-between items-center'>
        <h2>{item.taskName}</h2>
        <div>
          <button onClick={() => removeTask(item.taskId)} className='py-1 px-2 border-[1px]'><i className="las la-trash-alt"></i></button>
          <button onClick={() => editTask(item)} className='py-1 px-2 border-[1px] mx-3'><i className="las la-edit"></i></button>
          <button onClick={() => checkTask(item.taskId)} className='py-1 px-2 border-[1px]'><i className="las la-check"></i></button>
        </div>
      </div>
    })
  }

  // render todoList đã làm
  const renderTaskDone = () => {
    return taskList.filter(item => item.done).map((item, index) => {
      return <div key={index} className='border-[1px] p-2 flex justify-between items-center'>
        <h2>{item.taskName}</h2>
        <div>
          <button onClick={() => removeTask(item.taskId)} className='py-1 px-2 border-[1px]'><i className="las la-trash-alt"></i></button>
        </div>
      </div>
    })
  }
  return (
    <div className='flex items-center justify-center h-screen'>
      <div className='min-w-[700px] min-h-[500px] bg-gradient-to-b
      dark:from-purple-900 dark:to-blue-700 dark:text-white
      from-white to-pink-500 text-[#ff1493] p-5 rounded-[2px] border-[4px] border-blue-300 outline-none'>
        <select onChange={handleSetTheme} id='themeChange' className='w-full h-10 text-lg rounded-[2px] p-1 outline-none 
        bg-gradient-to-b dark:bg-purple-900 dark:to-blue-700 '>
          <option className='' value="dark">Theme Dark</option>
          <option value="light">Theme Light</option>
        </select>
        <h3 className='text-[40px]'>Todo List</h3>
        <div className='flex items-end'>
          <form onSubmit={formik.handleSubmit} className='w-8/12'>
            <label className='block' htmlFor="">Task name</label>
            <input onChange={formik.handleChange} value={formik.values.taskName}
              name='taskName'
              type="text" placeholder='Your task'
              className='p-1  border-[1px] outline-none text-black w-2/3 mr-2'
            />
            <button disabled={disableAddTask} className={`p-1 border-[1px] mx-2 border-white`}>
              <i className="las la-plus"></i>
              Add Task
            </button>
          </form>
          <div>
            <button disabled={disableUpdateTask} onClick={updateTask} className={`p-1 border-[1px] border-white`}>
              <i className="las la-upload"></i>
              Update Task
            </button>
          </div>
        </div>
        <div className='dark:text-lime-400 text-cyan-700 mt-1'>{formik.errors.taskName}</div>
        <h4 className='my-4 font-semibold text-[20px]'>Task todo</h4>
        <div>
          {renderTaskToDo()}
        </div>
        <div>
          <h4 className='my-4 font-semibold text-[20px]'>Task completed</h4>
          {renderTaskDone()}
        </div>
      </div>
    </div>
  )
}
