import { ADD_TASK, CHECK_TASK, EDIT_TASK, REMOVE_TASK, UPDATE_TASK } from "../contants/ToDoListConstannt"

export const addTaskAction = (taskName)=> {
    return {
        type: ADD_TASK,
        taskName
    }
}

export const removeTaskAction = (taskId) => {
    return {
        type: REMOVE_TASK,
        taskId
    }
}


export const updateTaskAction = (taskId, taskName) => {
    return {
        type: UPDATE_TASK, 
        taskId,
        taskName
    }
}


export const checkTaskAction = (taskId) => {
    return {
        type: CHECK_TASK,
        taskId
    }
}