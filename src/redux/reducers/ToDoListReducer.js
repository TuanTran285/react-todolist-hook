import { ADD_TASK, CHECK_TASK, EDIT_TASK, REMOVE_TASK, UPDATE_TASK } from "../contants/ToDoListConstannt"

const initialState = {
  taskList: [
    { taskId: 1, taskName: 'task 2', done: true },
    {taskId: 2, taskName: 'task 4', done: true },
  ]
}

const ToDoListReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK: {
      let newTaskList = [...state.taskList]
      newTaskList.push(action.taskName)
      state.taskList = newTaskList
      return { ...state }
    }
    case REMOVE_TASK: {
      let newTaskList = [...state.taskList]
      let index = newTaskList.findIndex(item => {
        return item.taskId === action.taskId
      })
      console.log(index)
      newTaskList.splice(index, 1)
      state.taskList = newTaskList
      return { ...state }
    }
    case UPDATE_TASK: {
      let newTaskList = [...state.taskList]
      let index = newTaskList.findIndex(item => {
        return item.taskId === action.taskId
      })
      if(index !== -1) {
        newTaskList[index].taskName = action.taskName
      }

      state.taskList = newTaskList
      return {...state}
    }
    case CHECK_TASK: {
      let newTaskList = [...state.taskList]
      let index = newTaskList.findIndex(item => item.taskId === action.taskId)
      if(index !== -1) {
        newTaskList[index].done = true
      }
      return {...state, taskList: newTaskList}
    }

    default:
      return state
  }
}

export default ToDoListReducer