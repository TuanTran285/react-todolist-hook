import {combineReducers} from 'redux'
import {createStore} from 'redux'
import ToDoListReducer from './reducers/ToDoListReducer'
const rootReducer = combineReducers({
    ToDoListReducer
})
const configStore = createStore(rootReducer) 
export default configStore